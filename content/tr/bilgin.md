+++
Author = "David T Freeman"
date = "2014-12-27T19:50:00Z"
groups = ["blog"]
keywords = ["Öğrendikçe", "oku"]
language = "tr"
tags = ["alıntı"]
title = "Bilgin"

+++

#### TR  
> Öğrendikçe, ne kadar az bildiğini fark edersin,  
> Ne kadar az bilirsen, o kadar çok bildiğini zannedersin.  

#### EN  
> The more you know, the more you realise how much you don’t know;  
> the less you know, the more you think you know.  

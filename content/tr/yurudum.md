+++
date = "2014-11-16T19:50:00Z"
description = "Ben Yürüdüm, Ama"
groups = ["blog"]
keywords = []
language = "tr"
title = "Yürüdüm"

+++

Ben Mutlulukla yürüdüm;  
Tüm yol gevezelikle geçti;  
Ama hiç bir şey öğrenmedim.  

Ben Üzüntüyle yürüdüm;  
Ve asla bir kelime etmedi;  
Ama, ah! Ondan öğrendiklerim varya.  